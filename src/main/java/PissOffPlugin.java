import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

// main class, extends the plugin class
public class PissOffPlugin extends JavaPlugin implements Listener {
    HashMap<Integer, String> listOfPlayersToCensor;
    HashMap<Integer, String> replacements;

    @Override
    public void onEnable() {
        super.onEnable();

        this.listOfPlayersToCensor = new HashMap<Integer, String>();
        this.replacements = new HashMap<Integer, String>();

        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("addphrase")) {
            // FIXME: we shouldn't concat anything in args array
            this.replacements.put(this.replacements.size(), String.join(" ", args));
        } else if (command.getName().equalsIgnoreCase("censor")) {
            this.listOfPlayersToCensor.put(this.listOfPlayersToCensor.size(), args[0]);
        }

        return true;
    }

    @EventHandler
    public void onChatMessage(AsyncPlayerChatEvent chatEvent) {
        // just ignore the synchronous events for now
        if (chatEvent.isAsynchronous()) {
            new MessageReplaceThread(chatEvent, this.listOfPlayersToCensor, this.replacements).run();
        }
    }
}
