import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashMap;
import java.util.Random;

public class MessageReplaceThread extends Thread {
    public HashMap<Integer, String> listOfPlayersToCensor;
    public HashMap<Integer, String> replacements;
    AsyncPlayerChatEvent chatEvent;

    MessageReplaceThread(AsyncPlayerChatEvent chatEvent, HashMap<Integer, String> listOfPlayersToCensor, HashMap<Integer, String> replacements) {
        this.chatEvent = chatEvent;
        this.listOfPlayersToCensor = listOfPlayersToCensor;
        this.replacements = replacements;
    }

    @Override
    public void run() {
        super.run();

        // create a new random number generator
        Random rng = new Random();

        // yes this is kind of a lambda but whatever
        if (this.listOfPlayersToCensor.containsValue(this.chatEvent.getPlayer().getName())) {
            this.chatEvent.setMessage(this.replacements.get(rng.nextInt(this.replacements.size())));
        }
    }
}
